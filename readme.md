# Curve Card

## Running the App

I haven't deployed this online anywhere but this should be runnable from any PHP7 install.

```
cp .env.example .env
cp phinx.yml.dist phinx.yml

composer install

vendor/bin/phpunit

vendor/bin/phinx migrate

# replace DB_DSN value in .env with /path/to/database/production.sqlite

composer start

# visit localhost:8080 in your browser!
```

#### Setup Etc.

Requirements:
* PHP 7+

Install

```
composer install
```

After install copy `.env.example` to `.env`, copy `phinx.yml.dist` for running migrations (`vendor/bin/phinx migrate`), 
and populate accordingly. Phinx needs a 'testing' database setup in order to run integration tests.  

Run tests

```
vendor/bin/phpunit

# builds code coverage report to build/coverage
composer coverage
```

Fix styles

```
vendor/bin/php-cs-fixer fix
```

## Approach Taken

This domain seems perfect for event sourcing, but I've never actually written any event
sourcing code before! There is some PHP tooling for event sourcing (Prooph, Broadway etc)
but I decided that I'd spend more time working out how to work with them than writing the
actual project, so I've gone for a 'roll your own approach'. For that reason this app might
not exactly by 'idiomatic' event sourcing.

I'm wiring together Slim for the webapp rather than Laravel or something similarly heavy, although
this proved a pain when it came to working with sqlite for testing (got the db setup wrong repeatedly!).

Most of the logic is contained in `Curve\Card\Account` which raises and plays events against
itself to cover the behaviour in the specification. I've not yet decided how I'm going to persist
these events but it will probably be through simple use of `serialize`.

Because the event sourcing system is very simple there's lots of things missing from this app
that would be needed in the real world, e.g:
* Concurrency support
* Conflict resolution
* Snapshots (or any separate read model at all at the moment)

I wasn't entirely clear from the spec how captures are meant to behave:

> The merchant can decide to only capture part of the amount or *capture the amount multiple times*. 

> In this model, the merchant *can’t capture more than we initially authorized him to*.

These seem to be conflicting statements? I have interpreted this as meaning once an authorization has been
captured in full it no longer has meaning, i.e my system does not allow for any recurring billing.

#### Frontend

Frontend is very basic, has no form validation etc! 

#### Exceptions For Flow Control

I suppose this could be considered a bad pattern, but here it lets me do simple failure states
without too much complex overhead. For Account events it could be replaced with a command 
success object.

#### Unit Testing

In unit tests I make quite a lot of use of `Money` objects and `Uuid` objects. Some people
would argue that these objects should be mocked, and that using real objects makes these tests
integration rather than unit tests. For the uuid it might as well be a language built in, 
and therefore I would argue it gets a free pass. For the money object it's a simple, immutable
value object with very simple and well defined behaviour. It could be mocked but that would 
substantially increase the test boilerplate for very little gain in my opinion. I have also 
found that mocking of simple and direct collaborating objects introduces bugs resulting from 
divergence in behaviour between the real object and the mock. 

## Spec `Event`

- [x] Open Account `Open`
- [x] Add funds `TopUp`
- [x] Authorize transactions `Authorize`
- [x] Reverse authorizations `Reverse`
- [x] Capture transactions `CaptureAuthorization`
- [x] Refund captures `Refund`
- [x] Persist events
- [x] Wrap in an app
- [x] Add transaction history to web app