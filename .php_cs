<?php

use PhpCsFixer\Config;
use PhpCsFixer\Finder;

$finder = Finder::create()
                ->exclude('vendor')
                ->in(__DIR__ . "/src")
                ->in(__DIR__ . "/tests");

return Config::create()
             ->setUsingCache(true)
             ->setRiskyAllowed(true)
             ->setRules([
                 '@PSR2' => true
             ])
             ->setFinder($finder);