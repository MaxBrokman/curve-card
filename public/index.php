<?php
if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

require __DIR__ . '/../vendor/autoload.php';

$dotenv = new Dotenv\Dotenv(__DIR__ . "/../");
$dotenv->load();

session_start();

$container = (new \Curve\Card\Container\ContainerFactory())->getContainer();

$app = new \Slim\App($container);

\Curve\Card\Http\AppContainer::setApp($app);

// Set up routes
require __DIR__ . '/../src/routes.php';

// Register helper functions
require __DIR__ . '/../src/helpers.php';

// Run app
$app->run();