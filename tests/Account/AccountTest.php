<?php


namespace tests\Curve\Card\Account;

use Curve\Card\Account\Account;
use Curve\Card\Account\AccountEvent;
use Curve\Card\Account\Authorize;
use Curve\Card\Account\CaptureAuthorization;
use Curve\Card\Account\Open;
use Curve\Card\Account\Refund;
use Curve\Card\Account\Reverse;
use Curve\Card\Account\TopUp;
use Curve\Card\InvalidEventException;
use Curve\Card\Money\Money;
use Curve\Card\UnknownEventException;
use Faker\Factory;
use Faker\Generator;
use Ramsey\Uuid\Uuid;
use tests\Curve\Card\MoneyAssertions;
use tests\Curve\Card\UnitTest;

class AccountTest extends UnitTest
{
    use MoneyAssertions;

    /**
     * @var Generator
     */
    private $faker;

    protected function setUp()
    {
        parent::setUp();

        $this->faker = Factory::create();
    }

    public function testEmptyAccount()
    {
        $account = $this->getOpenAccount();

        $this->assertZeroMoney($account->getTotalFunds(), 'GBP');
        $this->assertZeroMoney($account->getAuthorizedFunds(), 'GBP');
        $this->assertZeroMoney($account->getAvailableFunds(), 'GBP');

        $this->assertCount(0, $account->getEvents());
    }

    public function testOpenAccount()
    {
        $account = new Account();

        $id = Uuid::uuid4();
        $cardNumber = $this->faker->creditCardNumber();

        $account->open($id, $cardNumber);

        $this->assertCount(1, $account->getEvents());

        /** @var Open $lastEvent */
        $lastEvent = $account->getEvents()->last();

        $this->assertLastEventInstanceOf(Open::class, $account);
        $this->assertSame(1, $lastEvent->getVersion());

        $this->assertTrue($id->equals($account->getId()), "Account should have correct id");
        $this->assertEquals($cardNumber, $account->getCardNumber(), "Account should have correct card number");
        $this->assertSame('GBP', $account->getCurrencyCode(), "all accounts should be in GBP");

        $events = $account->popEvents();

        $this->assertCount(1, $events);
        $this->assertInstanceOf(Open::class, $events->first());

        $this->assertCount(0, $account->getEvents());
    }

    public function testOpenAccountTwice()
    {
        $this->expectException(InvalidEventException::class);

        $account = new Account();

        $account->open(Uuid::uuid4(), $this->faker->creditCardNumber());
        $account->open(Uuid::uuid4(), $this->faker->creditCardNumber());
    }

    public function testTopUp()
    {
        $account = $this->getOpenAccount();

        $account->topUp(new Money(100, 'GBP'));

        $this->assertLastEventInstanceOf(TopUp::class, $account);
        $this->assertMoneyMatching(100, 'GBP', $account->getTotalFunds());
    }

    public function testClosedAccountTopUp()
    {
        $this->expectException(InvalidEventException::class);

        $account = new Account();

        $account->topUp(new Money(100, 'GBP'));
    }

    public function testNegativeTopUp()
    {
        $this->expectException(InvalidEventException::class);

        $account = $this->getOpenAccount();

        $account->topUp(new Money(-100, 'GBP'));
    }

    public function testWrongCurrencyTopUp()
    {
        $this->expectException(InvalidEventException::class);

        $account = $this->getOpenAccount();

        $account->topUp(new Money(100, 'USD'));
    }

    public function testAuthorize()
    {
        $account = $this->getOpenAccount();

        $account->topUp(new Money(1000, 'GBP'));

        $merchantId = Uuid::uuid4();
        $transactionId = Uuid::uuid4();
        $amount = new Money(100, 'GBP');
        $account->authorize($merchantId, $transactionId, $amount);

        $this->assertLastEventInstanceOf(Authorize::class, $account);

        $this->assertMoneyMatching(1000, 'GBP', $account->getTotalFunds());
        $this->assertMoneyMatching(900, 'GBP', $account->getAvailableFunds());
        $this->assertMoneyMatching(100, 'GBP', $account->getAuthorizedFunds());
    }

    public function testAuthorizeWithoutAvailableFunds()
    {
        $this->expectException(InvalidEventException::class);

        $this->getOpenAccount()->authorize(Uuid::uuid4(), Uuid::uuid4(), new Money(100, 'GBP'));
    }

    public function testAuthorizeWhileClosed()
    {
        $this->expectException(InvalidEventException::class);

        (new Account())->authorize(Uuid::uuid4(), Uuid::uuid4(), new Money(100, 'GBP'));
    }

    public function testAuthorizeWrongCurrency()
    {
        $this->expectException(InvalidEventException::class);

        $this->getOpenAccount()->authorize(Uuid::uuid4(), Uuid::uuid4(), new Money(100, 'USD'));
    }

    public function testAuthorizeSameTransactionId()
    {
        $this->expectException(InvalidEventException::class);

        $account = $this->getOpenAccount();
        $account->topUp(new Money(1000, 'GBP'));

        $merchantId = Uuid::uuid4();
        $transactionId = Uuid::uuid4();

        $account->authorize($merchantId, $transactionId, new Money(100, 'GBP'));
        $account->authorize($merchantId, $transactionId, new Money(100, 'GBP'));
    }

    public function testReverse()
    {
        $account = $this->getOpenAccount();

        $account->topUp(new Money(1000, 'GBP'));

        $merchantId = Uuid::uuid4();
        $transactionId = Uuid::uuid4();

        $account->authorize($merchantId, $transactionId, new Money(100, 'GBP'));
        $account->reverse($merchantId, $transactionId, new Money(50, 'GBP'));

        $this->assertLastEventInstanceOf(Reverse::class, $account);

        $this->assertMoneyMatching(1000, 'GBP', $account->getTotalFunds());
        $this->assertMoneyMatching(950, 'GBP', $account->getAvailableFunds());
        $this->assertMoneyMatching(50, 'GBP', $account->getAuthorizedFunds());
    }

    public function testReverseClosedAccount()
    {
        $this->expectException(InvalidEventException::class);

        $account = new Account();

        $account->reverse(Uuid::uuid4(), Uuid::uuid4(), new Money(100, 'GBP'));
    }

    public function testReverseUnknownTransaction()
    {
        $this->expectException(InvalidEventException::class);

        $account = $this->getOpenAccount();
        $account->topUp(new Money(1000, 'GBP'));

        $account->reverse(Uuid::uuid4(), Uuid::uuid4(), new Money(10, 'GBP'));
    }

    public function testReverseWrongMerchant()
    {
        $this->expectException(InvalidEventException::class);

        $account = $this->getOpenAccount();
        $account->topUp(new Money(1000, 'GBP'));

        $merchantId = Uuid::uuid4();
        $transactionId = Uuid::uuid4();

        $account->authorize($merchantId, $transactionId, new Money(10, 'GBP'));

        // reverse with correct transaction but wrong merchant
        $account->reverse(Uuid::uuid4(), $transactionId, new Money(5, 'GBP'));
    }

    public function testReverseTooMuch()
    {
        $this->expectException(InvalidEventException::class);

        $account = $this->getOpenAccount();
        $account->topUp(new Money(1000, 'GBP'));

        $merchantId = Uuid::uuid4();
        $transactionId = Uuid::uuid4();

        $account->authorize($merchantId, $transactionId, new Money(10, 'GBP'));
        $account->reverse($merchantId, $transactionId, new Money(15, 'GBP'));
    }

    public function testReverseWrongCurrency()
    {
        $this->expectException(InvalidEventException::class);

        $account = $this->getOpenAccount();
        $account->topUp(new Money(1000, 'GBP'));

        $merchantId = Uuid::uuid4();
        $transactionId = Uuid::uuid4();

        $account->authorize($merchantId, $transactionId, new Money(10, 'GBP'));
        $account->reverse($merchantId, $transactionId, new Money(15, 'USD'));
    }

    public function testReverseCapturedFunds()
    {
        $this->expectException(InvalidEventException::class);

        $account = $this->getOpenAccount();

        $account->topUp(new Money(1000, 'GBP'));

        $merchantId = Uuid::uuid4();
        $transactionId = Uuid::uuid4();

        $account->authorize($merchantId, $transactionId, new Money(100, 'GBP'));
        $account->capture($merchantId, $transactionId, new Money(100, 'GBP'));
        $account->reverse($merchantId, $transactionId, new Money(50, 'GBP'));
    }

    public function testReplayBadReverse()
    {
        $this->expectException(\RuntimeException::class);

        $account = new Account();

        $account->replay(new Reverse(Uuid::uuid4(), 1, Uuid::uuid4(), Uuid::uuid4(), new Money(100, 'GBP')));
    }

    public function testCapture()
    {
        $account = $this->getOpenAccount();

        $account->topUp(new Money(1000, 'GBP'));

        $merchantId = Uuid::uuid4();
        $transactionId = Uuid::uuid4();

        $account->authorize($merchantId, $transactionId, new Money(100, 'GBP'));

        $account->capture($merchantId, $transactionId, new Money(20, 'GBP'));

        $this->assertLastEventInstanceOf(CaptureAuthorization::class, $account);

        $this->assertMoneyMatching(980, 'GBP', $account->getTotalFunds());
        $this->assertMoneyMatching(900, 'GBP', $account->getAvailableFunds());
        $this->assertMoneyMatching(80, 'GBP', $account->getAuthorizedFunds());

        // Lets try capturing some more!
        $account->capture($merchantId, $transactionId, new Money(20, 'GBP'));

        $this->assertMoneyMatching(960, 'GBP', $account->getTotalFunds());
        $this->assertMoneyMatching(900, 'GBP', $account->getAvailableFunds());
        $this->assertMoneyMatching(60, 'GBP', $account->getAuthorizedFunds());
    }

    public function testTotalCapture()
    {
        $account = $this->getOpenAccount();

        $account->topUp(new Money(1000, 'GBP'));

        $merchantId = Uuid::uuid4();
        $transactionId = Uuid::uuid4();

        $account->authorize($merchantId, $transactionId, new Money(100, 'GBP'));
        $account->capture($merchantId, $transactionId, new Money(100, 'GBP'));

        $this->assertMoneyMatching(900, 'GBP', $account->getTotalFunds());
        $this->assertMoneyMatching(900, 'GBP', $account->getAvailableFunds());
        $this->assertMoneyMatching(0, 'GBP', $account->getAuthorizedFunds());
    }

    public function testCaptureClosedAccount()
    {
        $this->expectException(InvalidEventException::class);

        $account = new Account();

        $account->capture(Uuid::uuid4(), Uuid::uuid4(), new Money(100, 'GBP'));
    }

    public function testCaptureUnauthorized()
    {
        $this->expectException(InvalidEventException::class);

        $account = $this->getOpenAccount();

        $account->capture(Uuid::uuid4(), Uuid::uuid4(), new Money(100, 'GBP'));
    }

    public function testCaptureWrongMerchant()
    {
        $this->expectException(InvalidEventException::class);

        $account = $this->getOpenAccount();

        $transactionId = Uuid::uuid4();

        $account->topUp(new Money(100, 'GBP'));
        $account->authorize(Uuid::uuid4(), $transactionId, new Money(50, 'GPB'));
        $account->capture(Uuid::uuid4(), $transactionId, new Money(50, 'GBP'));
    }

    public function testCaptureTooMuch()
    {
        $this->expectException(InvalidEventException::class);

        $account = $this->getOpenAccount();

        $transactionId = Uuid::uuid4();
        $merchantId = Uuid::uuid4();

        $account->topUp(new Money(100, 'GBP'));
        $account->authorize($merchantId, $transactionId, new Money(50, 'GBP'));
        $account->capture($merchantId, $transactionId, new Money(100, 'GBP'));
    }

    public function testCaptureReversed()
    {
        $this->expectException(InvalidEventException::class);

        $account = $this->getOpenAccount();

        $transactionId = Uuid::uuid4();
        $merchantId = Uuid::uuid4();

        $account->topUp(new Money(100, 'GBP'));
        $account->authorize($merchantId, $transactionId, new Money(50, 'GBP'));
        $account->reverse($merchantId, $transactionId, new Money(50, 'GBP'));
        $account->capture($merchantId, $transactionId, new Money(50, 'GBP'));
    }

    public function testCaptureWrongCurrency()
    {
        $this->expectException(InvalidEventException::class);

        $account = $this->getOpenAccount();

        $transactionId = Uuid::uuid4();
        $merchantId = Uuid::uuid4();

        $account->topUp(new Money(100, 'GBP'));
        $account->authorize($merchantId, $transactionId, new Money(100, 'GBP'));
        $account->capture($merchantId, $transactionId, new Money(50, 'USD'));
    }

    public function testReplayBadCapture()
    {
        $this->expectException(\RuntimeException::class);

        $account = $this->getOpenAccount();

        $account->replay(new CaptureAuthorization($account->getId(), 2, Uuid::uuid4(), Uuid::uuid4(), new Money(1000, 'GBP')));
    }

    public function testRefund()
    {
        $account = $this->getOpenAccount();

        $transactionId = Uuid::uuid4();
        $merchantId = Uuid::uuid4();

        $account->topUp(new Money(100, 'GBP'));
        $account->authorize($merchantId, $transactionId, new Money(50, 'GBP'));
        $account->capture($merchantId, $transactionId, new Money(50, 'GBP'));

        $account->refund($merchantId, $transactionId, new Money(50, 'GBP'));

        $this->assertLastEventInstanceOf(Refund::class, $account);

        $this->assertMoneyMatching(100, 'GBP', $account->getTotalFunds());
        $this->assertMoneyMatching(100, 'GBP', $account->getAvailableFunds());
        $this->assertMoneyMatching(0, 'GBP', $account->getAuthorizedFunds());
    }

    public function testRefundClosedAccount()
    {
        $this->expectException(InvalidEventException::class);

        (new Account)->refund(Uuid::uuid4(), Uuid::uuid4(), new Money(100, 'GBP'));
    }

    public function testRefundUnCaptured()
    {
        $this->expectException(InvalidEventException::class);

        $account = $this->getOpenAccount();

        $transactionId = Uuid::uuid4();
        $merchantId = Uuid::uuid4();

        $account->topUp(new Money(100, 'GBP'));
        $account->authorize($merchantId, $transactionId, new Money(50, 'GBP'));

        $account->refund($merchantId, $transactionId, new Money(50, 'GBP'));
    }

    public function testRefundTooMuch()
    {
        $this->expectException(InvalidEventException::class);

        $account = $this->getOpenAccount();

        $transactionId = Uuid::uuid4();
        $merchantId = Uuid::uuid4();

        $account->topUp(new Money(100, 'GBP'));
        $account->authorize($merchantId, $transactionId, new Money(50, 'GBP'));
        $account->capture($merchantId, $transactionId, new Money(50, 'GBP'));
        $account->refund($merchantId, $transactionId, new Money(100, 'GBP'));
    }

    public function testRefundWrongCurrency()
    {
        $this->expectException(InvalidEventException::class);

        $account = $this->getOpenAccount();

        $transactionId = Uuid::uuid4();
        $merchantId = Uuid::uuid4();

        $account->topUp(new Money(100, 'GBP'));
        $account->authorize($merchantId, $transactionId, new Money(50, 'GBP'));
        $account->capture($merchantId, $transactionId, new Money(50, 'GBP'));
        $account->refund($merchantId, $transactionId, new Money(50, 'USD'));
    }

    public function testRefundWrongMerchant()
    {
        $this->expectException(\InvalidArgumentException::class);

        $account = $this->getOpenAccount();

        $transactionId = Uuid::uuid4();
        $merchantId = Uuid::uuid4();

        $account->topUp(new Money(100, 'GBP'));
        $account->authorize($merchantId, $transactionId, new Money(50, 'GBP'));
        $account->capture($merchantId, $transactionId, new Money(50, 'GBP'));
        $account->refund(Uuid::uuid4(), $transactionId, new Money(50, 'GBP'));
    }

    public function testReplayUnknownEvent()
    {
        $this->expectException(UnknownEventException::class);

        $account = new Account();

        $account->replay(new FakeAccountEvent);
    }

    private function getOpenAccount()
    {
        $account = new Account();

        $account->replay(new Open(Uuid::uuid4(), $this->faker->creditCardNumber(), 1));

        return $account;
    }

    /**
     * @param string $class
     * @param Account $account
     */
    private static function assertLastEventInstanceOf($class, Account $account)
    {
        $lastEvent = $account->getEvents()->last();

        \PHPUnit_Framework_Assert::assertInstanceOf(AccountEvent::class, $lastEvent);
        \PHPUnit_Framework_Assert::assertInstanceOf($class, $lastEvent);
    }
}

class FakeAccountEvent extends AccountEvent
{
    public function __construct()
    {
        parent::__construct(Uuid::uuid4(), 1);
    }

    public function __toString(): string
    {
        return "test";
    }
}
