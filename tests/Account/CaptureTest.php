<?php


namespace tests\Curve\Card\Account;

use Curve\Card\Account\Capture;
use Curve\Card\Money\Money;
use Ramsey\Uuid\Uuid;
use tests\Curve\Card\MoneyAssertions;
use tests\Curve\Card\UnitTest;

class CaptureTest extends UnitTest
{
    use MoneyAssertions;

    public function testBasicAccess()
    {
        $merchantId = Uuid::uuid4();
        $transactionId = Uuid::uuid4();
        $amount = new Money(100, 'GBP');

        $capture = new Capture($merchantId, $transactionId, $amount);

        $this->assertSame($merchantId, $capture->getMerchantId(), 'Merchant id must match');
        $this->assertSame($transactionId, $capture->getTransactionId(), 'Transaction id must match');
        $this->assertMoneyMatching(100, 'GBP', $capture->getAmount());
    }

    public function testCapture()
    {
        $capture = $this->getCapture();

        $furtherCaptured = $capture->capture(new Money(10, 'GBP'));

        $this->assertInstanceOf(Capture::class, $furtherCaptured);
        $this->assertMoneyMatching(110, 'GBP', $furtherCaptured->getAmount());
        $this->assertEquals($capture->getMerchantId(), $furtherCaptured->getMerchantId(), 'Merchant id must match');
        $this->assertEquals($capture->getTransactionId(), $furtherCaptured->getTransactionId(), 'Transaction id must match');
    }

    public function testRefund()
    {
        $capture = $this->getCapture();

        $refunded = $capture->refund(new Money(10, 'GBP'));

        $this->assertInstanceOf(Capture::class, $refunded);
        $this->assertMoneyMatching(90, 'GBP', $refunded->getAmount());
        $this->assertEquals($capture->getMerchantId(), $refunded->getMerchantId(), 'Merchant id must match');
        $this->assertEquals($capture->getTransactionId(), $refunded->getTransactionId(), 'Transaction id must match');
    }

    private function getCapture()
    {
        return new Capture(Uuid::uuid4(), Uuid::uuid4(), new Money(100, 'GBP'));
    }
}
