<?php


namespace tests\Curve\Card\Account;

use Curve\Card\Account\Authorization;
use Curve\Card\Money\Money;
use Ramsey\Uuid\Uuid;
use tests\Curve\Card\MoneyAssertions;
use tests\Curve\Card\UnitTest;

class AuthorizationTest extends UnitTest
{
    use MoneyAssertions;

    public function testBasicAccess()
    {
        $eventId = Uuid::uuid4();
        $merchantId = Uuid::uuid4();
        $transactionId = Uuid::uuid4();
        $amount = new Money(100, 'GBP');

        $authorization = new Authorization($eventId, $merchantId, $transactionId, $amount);

        $this->assertSame($eventId, $authorization->getEventId(), 'Event id must match');
        $this->assertSame($merchantId, $authorization->getMerchantId(), 'Merchant id must match');
        $this->assertSame($transactionId, $authorization->getTransactionId(), 'Transaction id must match');
        $this->assertMoneyMatching(100, 'GBP', $authorization->getAmount());
    }

    public function testReverse()
    {
        $authorization = $this->getAuthorization();

        $reversed = $authorization->reverse(new Money(10, 'GBP'));

        $this->assertInstanceOf(Authorization::class, $reversed);
        $this->assertMoneyMatching(90, 'GBP', $reversed->getAmount());
        $this->assertEquals($authorization->getEventId(), $reversed->getEventId(), 'Event id must match');
        $this->assertEquals($authorization->getMerchantId(), $reversed->getMerchantId(), 'Merchant id must match');
        $this->assertEquals($authorization->getTransactionId(), $reversed->getTransactionId(), 'Transaction id must match');
    }

    public function testCapture()
    {
        $authorization = $this->getAuthorization();

        $captured = $authorization->capture(new Money(10, 'GBP'));

        $this->assertInstanceOf(Authorization::class, $captured);
        $this->assertMoneyMatching(90, 'GBP', $captured->getAmount());
        $this->assertEquals($authorization->getEventId(), $captured->getEventId(), 'Event id must match');
        $this->assertEquals($authorization->getMerchantId(), $captured->getMerchantId(), 'Merchant id must match');
        $this->assertEquals($authorization->getTransactionId(), $captured->getTransactionId(), 'Transaction id must match');
    }

    private function getAuthorization()
    {
        return new Authorization(Uuid::uuid4(), Uuid::uuid4(), Uuid::uuid4(), new Money(100, 'GBP'));
    }
}
