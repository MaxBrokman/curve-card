<?php


namespace tests\Curve\Card;

use Mockery as m;

abstract class UnitTest extends \PHPUnit_Framework_TestCase
{
    protected function tearDown()
    {
        parent::tearDown();

        m::close();
    }
}
