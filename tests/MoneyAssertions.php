<?php


namespace tests\Curve\Card;

use Curve\Card\Money\Money;

trait MoneyAssertions
{
    /**
     * @param int $amount
     * @param string $currencyCode
     * @param Money $actual
     */
    protected static function assertMoneyMatching($amount, $currencyCode, $actual)
    {
        \PHPUnit_Framework_Assert::assertInstanceOf(Money::class, $actual, 'Money object is of wrong class');
        \PHPUnit_Framework_Assert::assertSame($amount, $actual->getAmount(), 'Money object has wrong amount');
        \PHPUnit_Framework_Assert::assertSame($currencyCode, $actual->getCurrencyCode(), 'Money object is of wrong currency');
    }

    /**
     * @param Money $actual
     * @param string $currency
     */
    protected static function assertZeroMoney($actual, $currency = '')
    {
        \PHPUnit_Framework_Assert::assertInstanceOf(Money::class, $actual);
        \PHPUnit_Framework_Assert::assertSame(0, $actual->getAmount());

        if ($currency) {
            \PHPUnit_Framework_Assert::assertSame($currency, $actual->getCurrencyCode());
        }
    }
}
