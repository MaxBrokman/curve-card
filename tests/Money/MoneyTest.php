<?php


namespace tests\Curve\Card\Money;

use Curve\Card\Money\CurrencyMismatchException;
use Curve\Card\Money\Money;
use tests\Curve\Card\UnitTest;

class MoneyTest extends UnitTest
{
    public function testBasicUsage()
    {
        $money = new Money(100, 'GBP');

        $this->assertSame(100, $money->getAmount());
        $this->assertSame('GBP', $money->getCurrencyCode());
        $this->assertSame('1.00 GBP', $money->__toString());
    }

    public function testConstructFromBase()
    {
        $money = Money::fromBase(100.00, 'GBP');
        $this->assertSame(100 * 100, $money->getAmount());
    }

    public function testAdd()
    {
        $moneyOne = new Money(100, 'GBP');
        $moneyTwo = new Money(100, 'GBP');

        $additionResult = $moneyOne->add($moneyTwo);

        $this->assertInstanceOf(Money::class, $additionResult);
        $this->assertSame(200, $additionResult->getAmount());
        $this->assertSame('GBP', $additionResult->getCurrencyCode());

        // Addition is commutative
        $additionResult = $moneyTwo->add($moneyOne);

        $this->assertInstanceOf(Money::class, $additionResult);
        $this->assertSame(200, $additionResult->getAmount());
        $this->assertSame('GBP', $additionResult->getCurrencyCode());
    }

    public function testAddConflicting()
    {
        $this->expectException(CurrencyMismatchException::class);

        $moneyOne = new Money(100, 'GBP');
        $moneyTwo = new Money(100, 'USD');

        $moneyOne->add($moneyTwo);
    }

    public function testSub()
    {
        $moneyOne = new Money(100, 'GBP');
        $moneyTwo = new Money(100, 'GBP');

        $subtractionResult = $moneyOne->sub($moneyTwo);

        $this->assertInstanceOf(Money::class, $subtractionResult);
        $this->assertSame(0, $subtractionResult->getAmount());
        $this->assertSame('GBP', $subtractionResult->getCurrencyCode());

        // Subtraction of equal amounts is effectively commutative
        $subtractionResult = $moneyTwo->sub($moneyOne);

        $this->assertInstanceOf(Money::class, $subtractionResult);
        $this->assertSame(0, $subtractionResult->getAmount());
        $this->assertSame('GBP', $subtractionResult->getCurrencyCode());
    }

    public function testSubConflicting()
    {
        $this->expectException(CurrencyMismatchException::class);

        $moneyOne = new Money(100, 'GBP');
        $moneyTwo = new Money(100, 'USD');

        $moneyOne->sub($moneyTwo);
    }

    public function testNegate()
    {
        $money = new Money(100, 'GBP');

        $negated = $money->negate();

        $this->assertInstanceOf(Money::class, $negated);
        $this->assertSame(-100, $negated->getAmount());
        $this->assertSame('GBP', $negated->getCurrencyCode());
    }

    public function testEquals()
    {
        $moneyOne = new Money(100, 'GBP');
        $moneyTwo = new Money(100, 'GBP');

        $this->assertTrue($moneyOne->equals($moneyOne), "Money object should equal self");
        $this->assertTrue($moneyOne->equals($moneyTwo), "Money objects should be equal");
        $this->assertTrue($moneyTwo->equals($moneyOne), "Money objects should be equal");
    }

    public function testLessThan()
    {
        $moneyOne = new Money(101, 'GBP');
        $moneyTwo = new Money(99, 'GBP');

        $this->assertFalse($moneyOne->lt($moneyTwo), 'one should not be greater than two');
        $this->assertTrue($moneyTwo->lt($moneyOne), 'two should be greater than one');
    }
}
