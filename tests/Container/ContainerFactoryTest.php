<?php


namespace tests\Curve\Card\Container;

use Curve\Card\Container\ContainerFactory;
use League\Container\Container;
use tests\Curve\Card\UnitTest;

class ContainerFactoryTest extends UnitTest
{
    public function testGetsAContainer()
    {
        $factory = new ContainerFactory();

        $this->assertInstanceOf(Container::class, $factory->getContainer());
    }

    public function testContainerOnlyBuiltOnce()
    {
        $factory = new ContainerFactory();

        $containerOne = $factory->getContainer();
        $containerTwo = $factory->getContainer();

        $this->assertSame($containerOne, $containerTwo, 'containers should be same object');
    }

    /**
     * For running tests we need to be able to get a new container with new singletons
     * etc. Otherwise we hit lots of errors with PDO as the db changes underneath it.
     *
     * @test
     */
    public function testContainerCanBeForgotten()
    {
        $factory = new ContainerFactory();

        $containerOne = $factory->getContainer();

        $factory->reset();

        $containerTwo = $factory->getContainer();

        $this->assertNotSame($containerOne, $containerTwo, 'containers should not be same object');
    }
}
