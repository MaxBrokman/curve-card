<?php


namespace tests\Curve\Card\Persistence;

use Curve\Card\Account\Account;
use Curve\Card\Account\AccountEvent;
use Curve\Card\Account\AccountEventRepository;
use Curve\Card\Money\Money;
use Curve\Card\Persistence\SqlAccountEventRepository;
use Curve\Card\Persistence\SqlAccountRepository;
use Ramsey\Uuid\Uuid;
use tests\Curve\Card\IntegrationTest;

class SqlAccountEventRepositoryTest extends IntegrationTest
{
    /**
     * @var SqlAccountEventRepository
     */
    private $repository;

    /**
     * @var SqlAccountRepository
     */
    private $accountRepository;

    protected function setUp()
    {
        parent::setUp();

        $this->repository = $this->container->get(SqlAccountEventRepository::class);
        $this->accountRepository = $this->container->get(SqlAccountRepository::class);
    }

    public function testImplementsInterface()
    {
        $this->assertInstanceOf(AccountEventRepository::class, $this->repository);
    }

    public function testGetEventsFromSaveAccount()
    {
        $account = new Account();

        $accountId = Uuid::uuid4();

        $account->open($accountId, '0000111122223333');
        $account->topUp(new Money(100, 'GBP'));
        $account->authorize(Uuid::uuid4(), Uuid::uuid4(), new Money(1, 'GBP'));

        $this->accountRepository->save($account);

        $events = $this->repository->getEventsFor($accountId);

        $this->assertCount(3, $events);

        foreach ($events as $event) {
            $this->assertInstanceOf(AccountEvent::class, $event);
        }
    }
}
