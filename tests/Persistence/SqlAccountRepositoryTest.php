<?php


namespace tests\Curve\Card\Persistence;

use Curve\Card\Account\Account;
use Curve\Card\Account\AccountRepository;
use Curve\Card\Money\Money;
use Curve\Card\Persistence\SqlAccountRepository;
use Faker\Factory;
use Faker\Generator;
use Ramsey\Uuid\Uuid;
use tests\Curve\Card\IntegrationTest;

class SqlAccountRepositoryTest extends IntegrationTest
{
    /**
     * @var SqlAccountRepository
     */
    private $repository;

    /**
     * @var Generator
     */
    private $faker;

    protected function setUp()
    {
        parent::setUp();

        $this->repository = $this->container->get(SqlAccountRepository::class);
        $this->faker = Factory::create();
    }

    public function testImplementsInterface()
    {
        $this->assertInstanceOf(AccountRepository::class, $this->repository);
    }

    public function testCreateAndRebuildAccount()
    {
        $account = new Account();

        $accountId = Uuid::uuid4();
        $account->open($accountId, $this->faker->creditCardNumber());
        $account->topUp(new Money(100, 'GBP'));

        $this->repository->save($account);
        $retrievedAccount = $this->repository->find($accountId);

        $this->assertNotSame($account, $retrievedAccount);

        $this->assertInstanceOf(Account::class, $retrievedAccount);
        $this->assertEquals($account->getId(), $retrievedAccount->getId(), 'Id should match');
        $this->assertEquals($account->getTotalFunds(), $retrievedAccount->getTotalFunds(), 'total funds should match');
        $this->assertEquals($account->getAuthorizedFunds(), $retrievedAccount->getAuthorizedFunds(), 'authorized funds should match');
        $this->assertEquals($account->getAvailableFunds(), $retrievedAccount->getAvailableFunds(), 'available funds should match');
    }

    public function testFindNoAccount()
    {
        $this->assertTrue(is_null($this->repository->find(Uuid::uuid4())), 'No account should be find');
    }

    public function testFindOrCreateCreateCase()
    {
        $accountId  = Uuid::uuid4();
        $cardNumber = $this->faker->creditCardNumber();

        $account = $this->repository->findOrCreate($accountId, $cardNumber);

        $this->assertInstanceOf(Account::class, $account);
        $this->assertEquals($accountId, $account->getId(), 'Id should be one provided');
        $this->assertSame($cardNumber, $account->getCardNumber(), 'card number should be one provided');
    }

    public function testFindOrCreateFindCase()
    {
        $account = new Account();

        $accountId = Uuid::uuid4();
        $account->open($accountId, $this->faker->creditCardNumber());
        $account->topUp(new Money(100, 'GBP'));

        $this->repository->save($account);

        $retrievedAccount = $this->repository->findOrCreate($accountId, $this->faker->creditCardNumber());

        $this->assertNotSame($account, $retrievedAccount);

        $this->assertInstanceOf(Account::class, $retrievedAccount);
        $this->assertEquals($account->getId(), $retrievedAccount->getId(), 'Id should match');
        $this->assertEquals($account->getTotalFunds(), $retrievedAccount->getTotalFunds(), 'total funds should match');
        $this->assertEquals($account->getAuthorizedFunds(), $retrievedAccount->getAuthorizedFunds(), 'authorized funds should match');
        $this->assertEquals($account->getAvailableFunds(), $retrievedAccount->getAvailableFunds(), 'available funds should match');
    }
}
