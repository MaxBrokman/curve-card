<?php


namespace tests\Curve\Card\Persistence;

use Curve\Card\Account\AccountEvent;
use Curve\Card\Persistence\PersistableAccountEvent;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use tests\Curve\Card\UnitTest;

class PersistableAccountEventTest extends UnitTest
{
    public function testSerializeAndUnserializeEvent()
    {
        $event = new TestEvent(Uuid::uuid4(), 1, 'test');

        $persistable = PersistableAccountEvent::fromAccountEvent($event);

        $this->assertEquals($event->getId(), $persistable->getId(), 'Id must match');
        $this->assertEquals($event->getAccountId(), $persistable->getAccountId(), 'Account must match');
        $this->assertEquals($event->getVersion(), $persistable->getVersion(), 'version must match');

        $copiedPersistable = new PersistableAccountEvent($persistable->getId(), $persistable->getAccountId(),
            $persistable->getVersion(), $persistable->getSerializedEvent());

        /** @var TestEvent $rebuiltEvent */
        $rebuiltEvent = $copiedPersistable->toEvent();

        $this->assertInstanceOf(TestEvent::class, $rebuiltEvent);
        $this->assertEquals($event->getId(), $rebuiltEvent->getId(), 'Id must match');
        $this->assertEquals($event->getAccountId(), $rebuiltEvent->getAccountId(), 'Account must match');
        $this->assertEquals($event->getVersion(), $rebuiltEvent->getVersion(), 'version must match');
        $this->assertSame($event->getTestData(), $rebuiltEvent->getTestData(), 'test data must match');
    }
}

class TestEvent extends AccountEvent
{
    /**
     * @var string
     */
    private $testData;

    /**
     * TestEvent constructor.
     *
     * @param UuidInterface $accountId
     * @param int $version
     * @param string $testData
     */
    public function __construct(UuidInterface $accountId, int $version, string $testData)
    {
        parent::__construct($accountId, $version);

        $this->testData = $testData;
    }

    /**
     * @return string
     */
    public function getTestData(): string
    {
        return $this->testData;
    }

    public function __toString(): string
    {
        return 'test';
    }
}
