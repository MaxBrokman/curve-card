<?php


namespace tests\Curve\Card;

use Curve\Card\Container\ContainerFactory;
use Curve\Card\Http\AppContainer;
use League\Container\Container;
use Mockery as m;
use Phinx\Console\Command\Migrate;
use Phinx\Console\Command\Rollback;
use Psr\Http\Message\ResponseInterface;
use Slim\App;
use Slim\Http\Environment;
use Slim\Http\Request;
use Slim\Http\Response;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Yaml\Yaml;

abstract class IntegrationTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ContainerFactory
     */
    private $containerFactory;

    /**
     * @var Container
     */
    protected $container;

    protected function setUp()
    {
        parent::setUp();

        // set env with relative directory
        $dbPath = $this->getDbPath();
        $dsn = "sqlite:" . $dbPath;

        $_ENV['DB_DSN'] = $dsn;
        putenv("DB_DSN={$dsn}");

        $this->runMigrations();
        $this->containerFactory = new ContainerFactory();
        $this->container = $this->containerFactory->getContainer();
    }

    protected function tearDown()
    {
        parent::tearDown();

        $this->reverseMigrations();

        // Must clear the container to stop sqlite swapping out underneath PDO and making it unhappy
        $this->containerFactory->reset();

        m::close();
    }

    /**
     * Process the application given a request method and URI.
     *
     * Largely taken from slim skeleton
     *
     * @param string $requestMethod the request method (e.g. GET, POST, etc.)
     * @param string $requestUri the request URI
     * @param array|object|null $requestData the request data
     * @return ResponseInterface
     */
    public function runApp($requestMethod, $requestUri, $requestData = null)
    {
        // Create a mock environment for testing with
        $environment = Environment::mock(
            [
                'REQUEST_METHOD' => $requestMethod,
                'REQUEST_URI' => $requestUri
            ]
        );

        // Set up a request object based on the environment
        $request = Request::createFromEnvironment($environment);

        // Add request data, if it exists
        if (isset($requestData)) {
            $request = $request->withParsedBody($requestData);
        }

        // Set up a response object
        $response = new Response();

        // Instantiate the application
        $app = new App($this->container);

        AppContainer::setApp($app);

        // Register routes and helpers
        require __DIR__ . '/../src/routes.php';
        require_once __DIR__ . '/../src/helpers.php';

        // Process the application
        $response = $app->process($request, $response);

        // Return the response
        return $response;
    }

    /**
     * This relies on your phinx.yml having a testing env!
     */
    protected function runMigrations()
    {
        $command = new Migrate();
        $input = new ArrayInput(['--environment' => 'testing']);
        $output = new NullOutput();
        $command->run($input, $output);
    }

    /**
     * This relies on your phinx.yml having a testing env!
     */
    private function reverseMigrations()
    {
        $command = new Rollback();
        $input = new ArrayInput(['--environment' => 'testing', '--target' => 0]);
        $output = new NullOutput();
        $command->run($input, $output);
    }

    /**
     * @return string
     */
    protected function getDbPath(): string
    {
        $phinxConfigPath = __DIR__ . "/../phinx.yml";

        if (!file_exists($phinxConfigPath)) {
            throw new \PHPUnit_Framework_SkippedTestError("Could not determine path to phinx config, skipping integration test");
        }

        $config = Yaml::parse(file_get_contents($phinxConfigPath));

        if (!isset($config['environments']['testing']['adapter']) || !isset($config['environments']['testing']['name'])) {
            throw new \PHPUnit_Framework_SkippedTestError("Could not read testing Db data from phinx config. Please provide a 'testing' environment setting, skipping integration test");
        }

        $adapter = $config['environments']['testing']['adapter'];
        $name = $config['environments']['testing']['name'];

        if ($adapter !== 'sqlite') {
            throw new \PHPUnit_Framework_SkippedTestError("Integration tests intended to run against sqlite, skipping integration test");
        }

        $dbPath = __DIR__ . "/../" . $name;

        return $dbPath;
    }
}
