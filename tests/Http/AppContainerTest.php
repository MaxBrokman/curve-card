<?php


namespace tests\Curve\Card\Http;

use Curve\Card\Http\AppContainer;
use Mockery as m;
use Slim\App;
use tests\Curve\Card\UnitTest;

class AppContainerTest extends UnitTest
{
    protected function setUp()
    {
        parent::setUp();

        AppContainer::reset();
    }

    protected function tearDown()
    {
        parent::tearDown();

        AppContainer::reset();
    }


    public function testHoldsAReferenceToApp()
    {
        $app = m::mock(App::class);

        AppContainer::setApp($app);
        $retrievedReference = AppContainer::getApp();

        $this->assertSame($app, $retrievedReference);
    }

    public function testResets()
    {
        $this->expectException(\RuntimeException::class);

        $app = m::mock(App::class);

        AppContainer::setApp($app);
        AppContainer::reset();

        $this->assertNull(AppContainer::getApp());
    }

    public function testFailsIfNotSet()
    {
        $this->expectException(\RuntimeException::class);

        AppContainer::getApp();
    }
}
