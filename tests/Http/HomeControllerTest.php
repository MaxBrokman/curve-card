<?php


namespace tests\Curve\Card\Http;

use tests\Curve\Card\IntegrationTest;

class HomeControllerTest extends IntegrationTest
{
    public function testHomeRenders()
    {
        $response = $this->runApp("GET", "/", []);

        $this->assertSame(200, $response->getStatusCode(), 'response should be 200');
    }
}
