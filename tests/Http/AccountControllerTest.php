<?php


namespace tests\Curve\Card\Http;

use Curve\Card\Account\AccountRepository;
use Curve\Card\Money\Money;
use Ramsey\Uuid\Uuid;
use tests\Curve\Card\IntegrationTest;
use tests\Curve\Card\MoneyAssertions;

class AccountControllerTest extends IntegrationTest
{
    use MoneyAssertions;

    /**
     * @var AccountRepository
     */
    private $accounts;

    protected function setUp()
    {
        parent::setUp();

        $this->accounts = $this->container->get(AccountRepository::class);
    }


    public function testGetAccountHomePage()
    {
        $response = $this->runApp('get', "/" . Uuid::uuid4()->toString());

        $this->assertSame(200, $response->getStatusCode());
    }

    public function testVisitingAccountPageCreatesAccount()
    {
        $accountId = Uuid::uuid4();
        $response  = $this->runApp('get', "/" . $accountId->toString());

        $this->assertSame(200, $response->getStatusCode());

        $this->assertFalse(is_null($this->accounts->find($accountId)), 'visiting account page should have created the account');
    }

    public function testCanDisplayAllEvents()
    {
        $accountId = Uuid::uuid4();
        $merchantId = Uuid::uuid4();
        $transactionId = Uuid::uuid4();
        $cardNumber = '0000000000000000';

        $account = $this->accounts->findOrCreate($accountId, $cardNumber);

        $account->topUp(new Money(10000, 'GBP'));
        $account->authorize($merchantId, $transactionId, new Money(1000, 'GBP'));
        $account->reverse($merchantId, $transactionId, new Money(500, 'GBP'));
        $account->capture($merchantId, $transactionId, new Money(100, 'GBP'));
        $account->refund($merchantId, $transactionId, new Money(50, 'GBP'));

        $this->accounts->save($account);

        $response = $this->runApp('get', "/" . $accountId->toString());

        $this->assertSame(200, $response->getStatusCode(), "response must be 200");
    }

    public function testBadUuidIsError()
    {
        $response = $this->runApp('get', "/notuuid");

        $this->assertSame(404, $response->getStatusCode());
    }

    public function testTopUp()
    {
        $accountId = Uuid::uuid4();
        $cardNumber = '0000000000000000';

        $this->accounts->findOrCreate($accountId, $cardNumber);

        $response = $this->runApp('post', "/" . $accountId->toString() . "/topup", [
            'topup-amount' => 100
        ]);

        $this->assertSame(302, $response->getStatusCode(), 'Response should be a redirect');

        $account = $this->accounts->find($accountId);

        $this->assertMoneyMatching(10000, 'GBP', $account->getTotalFunds());
    }

    public function testTopUpBadAccount()
    {
        $accountId = Uuid::uuid4();

        $response = $this->runApp('post', "/" . $accountId->toString() . "/topup", [
            'topup-amount' => 100
        ]);

        $this->assertSame(404, $response->getStatusCode(), 'Response should be a 404');

        $accountId = 'notAUuid';

        $response = $this->runApp('post', "/" . $accountId . "/topup", [
            'topup-amount' => 100
        ]);

        $this->assertSame(404, $response->getStatusCode(), 'Response should be a 404');
    }

    public function testTopUpNegative()
    {
        $accountId = Uuid::uuid4();
        $cardNumber = '0000000000000000';

        $this->accounts->findOrCreate($accountId, $cardNumber);

        $response = $this->runApp('post', "/" . $accountId->toString() . "/topup", [
            'topup-amount' => -100
        ]);

        $this->assertSame(400, $response->getStatusCode(), 'Response should be a redirect');
    }

    public function testAuthorize()
    {
        $accountId = Uuid::uuid4();
        $cardNumber = '0000000000000000';

        $account = $this->accounts->findOrCreate($accountId, $cardNumber);

        $account->topUp(new Money(10000, 'GBP'));
        $this->accounts->save($account);

        $response = $this->runApp('post', "/" . $accountId->toString() . "/authorize", [
            'authorize-amount' => 10
        ]);

        $this->assertSame(302, $response->getStatusCode(), 'Response should be a redirect');

        $account = $this->accounts->find($accountId);

        $this->assertMoneyMatching(10000, 'GBP', $account->getTotalFunds());
        $this->assertMoneyMatching(1000, 'GBP', $account->getAuthorizedFunds());
        $this->assertMoneyMatching(9000, 'GBP', $account->getAvailableFunds());
    }

    public function testAuthorizeBadAccount()
    {
        $accountId = Uuid::uuid4();

        $response = $this->runApp('post', "/" . $accountId->toString() . "/authorize", [
            'authorize-amount' => 100
        ]);

        $this->assertSame(404, $response->getStatusCode(), 'Response should be a 404');

        $accountId = 'notAUuid';

        $response = $this->runApp('post', "/" . $accountId . "/authorize", [
            'authorize-amount' => 100
        ]);

        $this->assertSame(404, $response->getStatusCode(), 'Response should be a 404');
    }

    public function testIllegalAuthorize()
    {
        $accountId = Uuid::uuid4();
        $cardNumber = '0000000000000000';

        $this->accounts->findOrCreate($accountId, $cardNumber);

        $response = $this->runApp('post', "/" . $accountId->toString() . "/authorize", [
            'authorize-amount' => 100
        ]);

        $this->assertSame(400, $response->getStatusCode(), 'Response should be a redirect');
    }

    public function testCapture()
    {
        $accountId = Uuid::uuid4();
        $merchantId = Uuid::uuid4();
        $transactionId = Uuid::uuid4();
        $cardNumber = '0000000000000000';

        $account = $this->accounts->findOrCreate($accountId, $cardNumber);

        $account->topUp(new Money(10000, 'GBP'));
        $account->authorize($merchantId, $transactionId, new Money(1000, 'GBP'));

        $this->accounts->save($account);

        $response = $this->runApp('post', "/" . $accountId->toString() . "/capture", [
            'capture-amount' => 10,
            'transaction-id' => $transactionId->toString(),
            'merchant-id' => $merchantId->toString()
        ]);

        $this->assertSame(302, $response->getStatusCode(), 'Response should be a redirect');

        $account = $this->accounts->find($accountId);

        $this->assertMoneyMatching(9000, 'GBP', $account->getTotalFunds());
        $this->assertMoneyMatching(0, 'GBP', $account->getAuthorizedFunds());
        $this->assertMoneyMatching(9000, 'GBP', $account->getAvailableFunds());
    }

    public function testCaptureBadAccount()
    {
        $accountId = Uuid::uuid4();

        $response = $this->runApp('post', "/" . $accountId->toString() . "/capture", [
            'authorize-amount' => 100
        ]);

        $this->assertSame(404, $response->getStatusCode(), 'Response should be a 404');

        $accountId = 'notAUuid';

        $response = $this->runApp('post', "/" . $accountId . "/capture", [
            'authorize-amount' => 100
        ]);

        $this->assertSame(404, $response->getStatusCode());
    }

    public function testIllegalCapture()
    {
        $accountId = Uuid::uuid4();
        $merchantId = Uuid::uuid4();
        $transactionId = Uuid::uuid4();
        $cardNumber = '0000000000000000';

        $account = $this->accounts->findOrCreate($accountId, $cardNumber);

        $account->topUp(new Money(10000, 'GBP'));
        $account->authorize($merchantId, $transactionId, new Money(1000, 'GBP'));

        $this->accounts->save($account);

        $response = $this->runApp('post', "/" . $accountId->toString() . "/capture", [
            'capture-amount' => 10,
            'transaction-id' => $transactionId->toString(),
            'merchant-id' => Uuid::uuid4(),
        ]);

        $this->assertSame(400, $response->getStatusCode(), 'Response should be an error');
    }

    public function testMalformedCapture()
    {
        $accountId = Uuid::uuid4();
        $merchantId = Uuid::uuid4();
        $transactionId = Uuid::uuid4();
        $cardNumber = '0000000000000000';

        $account = $this->accounts->findOrCreate($accountId, $cardNumber);

        $account->topUp(new Money(10000, 'GBP'));
        $account->authorize($merchantId, $transactionId, new Money(1000, 'GBP'));

        $this->accounts->save($account);

        $response = $this->runApp('post', "/" . $accountId->toString() . "/capture", [
            'capture-amount' => 10,
            'transaction-id' => $transactionId->toString(),
            'merchant-id' => 'notuuid',
        ]);

        $this->assertSame(400, $response->getStatusCode(), 'Response should be an error');
    }

    public function testReverse()
    {
        $accountId = Uuid::uuid4();
        $merchantId = Uuid::uuid4();
        $transactionId = Uuid::uuid4();
        $cardNumber = '0000000000000000';

        $account = $this->accounts->findOrCreate($accountId, $cardNumber);

        $account->topUp(new Money(10000, 'GBP'));
        $account->authorize($merchantId, $transactionId, new Money(1000, 'GBP'));

        $this->accounts->save($account);

        $response = $this->runApp('post', "/" . $accountId->toString() . "/reverse", [
            'reverse-amount' => 5,
            'transaction-id' => $transactionId->toString(),
            'merchant-id' => $merchantId->toString()
        ]);

        $this->assertSame(302, $response->getStatusCode(), 'Response should be a redirect');

        $account = $this->accounts->find($accountId);

        $this->assertMoneyMatching(10000, 'GBP', $account->getTotalFunds());
        $this->assertMoneyMatching(500, 'GBP', $account->getAuthorizedFunds());
        $this->assertMoneyMatching(9500, 'GBP', $account->getAvailableFunds());
    }

    public function testReverseBadAccount()
    {
        $accountId = Uuid::uuid4();

        $response = $this->runApp('post', "/" . $accountId->toString() . "/reverse", [
            'reverse-amount' => 100
        ]);

        $this->assertSame(404, $response->getStatusCode(), 'Response should be a 404');

        $accountId = 'notAUuid';

        $response = $this->runApp('post', "/" . $accountId . "/reverse", [
            'reverse-amount' => 100
        ]);

        $this->assertSame(404, $response->getStatusCode());
    }

    public function testIllegalReverse()
    {
        $accountId = Uuid::uuid4();
        $merchantId = Uuid::uuid4();
        $transactionId = Uuid::uuid4();
        $cardNumber = '0000000000000000';

        $account = $this->accounts->findOrCreate($accountId, $cardNumber);

        $account->topUp(new Money(10000, 'GBP'));
        $account->authorize($merchantId, $transactionId, new Money(1000, 'GBP'));

        $this->accounts->save($account);

        $response = $this->runApp('post', "/" . $accountId->toString() . "/reverse", [
            'reverse-amount' => 10000,
            'transaction-id' => $transactionId->toString(),
            'merchant-id' => $merchantId->toString(),
        ]);

        $this->assertSame(400, $response->getStatusCode(), 'Response should be an error');
    }

    public function testRefund()
    {
        $accountId = Uuid::uuid4();
        $merchantId = Uuid::uuid4();
        $transactionId = Uuid::uuid4();
        $cardNumber = '0000000000000000';

        $account = $this->accounts->findOrCreate($accountId, $cardNumber);

        $account->topUp(new Money(10000, 'GBP'));
        $account->authorize($merchantId, $transactionId, new Money(1000, 'GBP'));
        $account->capture($merchantId, $transactionId, new Money(1000, 'GBP'));

        $this->accounts->save($account);

        $response = $this->runApp('post', "/" . $accountId->toString() . "/refund", [
            'refund-amount' => 10,
            'transaction-id' => $transactionId->toString(),
            'merchant-id' => $merchantId->toString()
        ]);

        $this->assertSame(302, $response->getStatusCode(), 'Response should be a redirect');

        $account = $this->accounts->find($accountId);

        $this->assertMoneyMatching(10000, 'GBP', $account->getTotalFunds());
        $this->assertMoneyMatching(0, 'GBP', $account->getAuthorizedFunds());
        $this->assertMoneyMatching(10000, 'GBP', $account->getAvailableFunds());
    }

    public function testRefundBadAccount()
    {
        $accountId = Uuid::uuid4();

        $response = $this->runApp('post', "/" . $accountId->toString() . "/refund", [
            'refund-amount' => 100
        ]);

        $this->assertSame(404, $response->getStatusCode(), 'Response should be a 404');

        $accountId = 'notAUuid';

        $response = $this->runApp('post', "/" . $accountId . "/refund", [
            'refund-amount' => 100
        ]);

        $this->assertSame(404, $response->getStatusCode());
    }

    public function testIllegalRefund()
    {
        $accountId = Uuid::uuid4();
        $merchantId = Uuid::uuid4();
        $transactionId = Uuid::uuid4();
        $cardNumber = '0000000000000000';

        $account = $this->accounts->findOrCreate($accountId, $cardNumber);

        $account->topUp(new Money(10000, 'GBP'));

        $this->accounts->save($account);

        $response = $this->runApp('post', "/" . $accountId->toString() . "/refund", [
            'refund-amount' => 10,
            'transaction-id' => $transactionId->toString(),
            'merchant-id' => $merchantId->toString()
        ]);

        $this->assertSame(400, $response->getStatusCode(), 'Response should be an error');
    }
}
