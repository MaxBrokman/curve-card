<?php


namespace Curve\Card\Http;

use Psr\Http\Message\ResponseInterface;
use Ramsey\Uuid\Uuid;
use Slim\Http\Request;

class HomeController
{
    public function splash(Request $request, ResponseInterface $response, array $args): ResponseInterface
    {
        return view()->render($response, "splash.html", [
            "newAccountLink" => route("accountHome", ["accountId" => Uuid::uuid4()->toString()]),
        ]);
    }
}
