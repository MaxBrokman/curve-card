<?php
declare(strict_types=1);

namespace Curve\Card\Http;

use Curve\Card\Account\AccountEvent;
use Curve\Card\Account\Authorize;
use Curve\Card\Account\CaptureAuthorization;

class AccountEventTransformer
{
    /**
     * @param AccountEvent $event
     *
     * @return array
     */
    public function transform(AccountEvent $event): array
    {
        return [
            "type"    => $this->getType($event),
            "message" => $event->__toString(),
            "actions" => $this->getActions($event),
            "merchantId" => method_exists($event, 'getMerchantId') ? $event->getMerchantId()->toString() : null,
            "transactionId" => method_exists($event, 'getTransactionId') ? $event->getTransactionId()->toString() : null,
        ];
    }

    /**
     * @param AccountEvent $event
     *
     * @return string
     */
    private function getType(AccountEvent $event): string
    {
        // This could be optimised by just doing a match on the FQN of the event (reflection is slow etc)
        // but not too concerned about performance here.
        $reflect = new \ReflectionObject($event);
        return strtolower($reflect->getShortName());
    }

    /**
     * @param $event
     *
     * @return array
     */
    private function getActions($event): array
    {
        switch (get_class($event)) {
            case CaptureAuthorization::class:
                /** @var CaptureAuthorization $event */
                return [
                    "refundAction" => route("accountRefund", ["accountId" => $event->getAccountId()]),
                ];

            case Authorize::class:
                /** @var Authorize $event */
                return [
                    "captureAction" => route("accountCapture", ["accountId" => $event->getAccountId()]),
                    "reverseAction" => route("accountReverse", ["accountId" => $event->getAccountId()]),
                ];
        }

        return [];
    }
}
