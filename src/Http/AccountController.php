<?php
declare(strict_types=1);

namespace Curve\Card\Http;

use Curve\Card\Account\Account;
use Curve\Card\Account\AccountEvent;
use Curve\Card\Account\AccountEventRepository;
use Curve\Card\Account\AccountRepository;
use Curve\Card\InvalidEventException;
use Curve\Card\Money\Money;
use Faker\Factory;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Ramsey\Uuid\Uuid;
use Slim\Http\Request;

class AccountController
{
    /**
     * @var AccountRepository
     */
    private $accounts;

    /**
     * @var AccountEventRepository
     */
    private $accountEvents;

    /**
     * @var AccountEventTransformer
     */
    private $eventTransformer;

    /**
     * AccountController constructor.
     *
     * @param AccountRepository $accounts
     * @param AccountEventRepository $accountEvents
     * @param AccountEventTransformer $eventTransformer
     */
    public function __construct(
        AccountRepository $accounts,
        AccountEventRepository $accountEvents,
        AccountEventTransformer $eventTransformer
    ) {
        $this->accounts         = $accounts;
        $this->accountEvents    = $accountEvents;
        $this->eventTransformer = $eventTransformer;
    }

    /**
     * Home page for an account, returns nothing right now but is the entry point to the app!
     *
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @param array $args
     *
     * @return ResponseInterface
     */
    public function home(RequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        try {
            $accountId = Uuid::fromString($args['accountId']);
        } catch (\InvalidArgumentException $e) {
            return $response->withStatus(404);
        }

        $account = $this->accounts->findOrCreate($accountId, $this->getCardNumber());

        // Reverse so events are in descending order
        $events = array_reverse($this->accountEvents->getEventsFor($accountId));

        return view()->render($response, 'accountHome.html', [
            'account'         => $account,
            'events'          => array_map(function (AccountEvent $event) {
                return $this->eventTransformer->transform($event);
            }, $events),
            'topupAction'     => route('accountTopup', [
                'accountId' => $accountId->toString(),
            ]),
            'authorizeAction' => route('accountAuthorize', [
                'accountId' => $accountId->toString(),
            ]),
        ]);
    }

    /**
     * @param Request $request
     * @param ResponseInterface $response
     * @param array $args
     *
     * @return ResponseInterface
     */
    public function topUp(Request $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $then = function (Account $account, Request $request) {
            $topUpAmount = floatval($request->getParam('topup-amount', 0.0));
            $account->topUp(Money::fromBase($topUpAmount, $account->getCurrencyCode()));
        };

        return $this->findAccountAnd($request, $response, $args, $then);
    }

    /**
     * @param Request $request
     * @param ResponseInterface $response
     * @param array $args
     *
     * @return ResponseInterface
     */
    public function authorize(Request $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $then = function (Account $account, Request $request) {
            $authorizeAmount = floatval($request->getParam('authorize-amount', 0.0));
            $account->authorize(Uuid::uuid4(), Uuid::uuid4(),
                Money::fromBase($authorizeAmount, $account->getCurrencyCode()));
        };

        return $this->findAccountAnd($request, $response, $args, $then);
    }

    /**
     * @param Request $request
     * @param ResponseInterface $response
     * @param array $args
     *
     * @return ResponseInterface
     */
    public function capture(Request $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $then = function (Account $account, Request $request) {
            $merchantId    = Uuid::fromString($request->getParam('merchant-id'));
            $transactionId = Uuid::fromString($request->getParam('transaction-id'));
            $amount        = floatval($request->getParam('capture-amount', 0.0));

            $account->capture($merchantId, $transactionId, Money::fromBase($amount, $account->getCurrencyCode()));
        };

        return $this->findAccountAnd($request, $response, $args, $then);
    }

    /**
     * @param Request $request
     * @param ResponseInterface $response
     * @param array $args
     *
     * @return ResponseInterface
     */
    public function reverse(Request $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $then = function (Account $account, Request $request) {
            $merchantId    = Uuid::fromString($request->getParam('merchant-id'));
            $transactionId = Uuid::fromString($request->getParam('transaction-id'));
            $amount        = floatval($request->getParam('reverse-amount', 0.0));

            $account->reverse($merchantId, $transactionId, Money::fromBase($amount, $account->getCurrencyCode()));
        };

        return $this->findAccountAnd($request, $response, $args, $then);
    }

    /**
     * @param Request $request
     * @param ResponseInterface $response
     * @param array $args
     *
     * @return ResponseInterface
     */
    public function refund(Request $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $then = function (Account $account, Request $request) {
            $merchantId    = Uuid::fromString($request->getParam('merchant-id'));
            $transactionId = Uuid::fromString($request->getParam('transaction-id'));
            $amount        = floatval($request->getParam('refund-amount', 0.0));

            $account->refund($merchantId, $transactionId, Money::fromBase($amount, $account->getCurrencyCode()));
        };

        return $this->findAccountAnd($request, $response, $args, $then);
    }

    /**
     * Convenience wrapper that does boilerplate around account commands
     *
     * @param Request $request
     * @param ResponseInterface $response
     * @param array $args
     * @param \Closure $then, receives $account, $request
     *
     * @return ResponseInterface
     */
    private function findAccountAnd(
        Request $request,
        ResponseInterface $response,
        array $args,
        \Closure $then
    ): ResponseInterface {
        try {
            $accountId = Uuid::fromString($args['accountId']);
        } catch (\InvalidArgumentException $e) {
            return $response->withStatus(404);
        }

        $account = $this->accounts->find($accountId);

        if (!$account) {
            return $response->withStatus(404);
        }

        try {
            $then($account, $request);
        } catch (InvalidEventException $e) {
            return view()->render($response, 'accountError.html', [
                'error' => $e->getMessage(),
            ])->withStatus(400);
        } catch (\InvalidArgumentException $e) {
            return $response->withStatus(400);
        }

        $this->accounts->save($account);

        return $response->withStatus(302)->withHeader('Location', $this->getAccountHomeUrl($account) . "#account-history");
    }

    /**
     * @return string
     */
    private function getCardNumber(): string
    {
        return Factory::create()->creditCardNumber();
    }

    /**
     * @param Account $account
     *
     * @return string
     */
    private function getAccountHomeUrl(Account $account): string
    {
        return route('accountHome', [
            'accountId' => $account->getId()->toString(),
        ]);
    }
}
