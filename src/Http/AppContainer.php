<?php
declare(strict_types=1);

namespace Curve\Card\Http;

use Slim\App;

/**
 * A little bit better than a global variable!
 *
 * Class AppContainer
 * @package Curve\Card\Http
 */
class AppContainer
{
    /**
     * @var App
     */
    private static $app;

    /**
     * @param App $app
     */
    public static function setApp(App $app)
    {
        static::$app = $app;
    }

    /**
     * @return App
     */
    public static function getApp(): App
    {
        if (!static::$app) {
            throw new \RuntimeException("AppContainer has not been initialised");
        }

        return static::$app;
    }

    /**
     * Test helper
     */
    public static function reset()
    {
        static::$app = null;
    }
}
