<?php
declare(strict_types=1);

namespace Curve\Card\Account;

use Curve\Card\InvalidEventException;
use Curve\Card\Money\CurrencyMismatchException;
use Curve\Card\Money\Money;
use Curve\Card\UnknownEventException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Ramsey\Uuid\UuidInterface;

class Account
{
    /**
     * @var UuidInterface
     */
    private $id;

    /**
     * @var string
     */
    private $cardNumber;

    /**
     * @var string
     */
    private $currencyCode;

    /**
     * @var Money
     */
    private $totalFunds;

    /**
     * @var Money
     */
    private $authorizedFunds;

    /**
     * @var Authorization[]|Collection
     */
    private $authorizedTransactions;

    /**
     * @var Capture[]|Collection
     */
    private $capturedTransactions;

    /**
     * @var Collection|AccountEvent[]
     */
    private $events;

    /**
     * @var int
     */
    private $version;

    /**
     * Account constructor.
     */
    public function __construct()
    {
        $this->totalFunds = new Money(0, '');
        $this->authorizedFunds = new Money(0, '');

        $this->authorizedTransactions = new ArrayCollection();
        $this->capturedTransactions = new ArrayCollection();

        $this->events = new ArrayCollection();
        $this->version = 0;
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCardNumber(): string
    {
        return $this->cardNumber;
    }

    /**
     * @return string
     */
    public function getCurrencyCode(): string
    {
        return $this->currencyCode;
    }

    /**
     * @return Money
     */
    public function getTotalFunds(): Money
    {
        return $this->totalFunds;
    }

    /**
     * @return Money
     */
    public function getAuthorizedFunds(): Money
    {
        return $this->authorizedFunds;
    }

    /**
     * @return Money
     */
    public function getAvailableFunds(): Money
    {
        return $this->totalFunds->sub($this->authorizedFunds);
    }

    /**
     * @return bool
     */
    public function isOpen(): bool
    {
        return !is_null($this->id);
    }

    /**
     * @param UuidInterface $id
     * @param string $cardNumber
     */
    public function open(UuidInterface $id, string $cardNumber)
    {
        $this->play(new Open($id, $cardNumber, $this->version + 1));
    }

    /**
     * @param Open $open
     */
    public function onOpen(Open $open)
    {
        if ($this->id) {
            throw new InvalidEventException("Account is already open");
        }

        $this->id = $open->getAccountId();
        $this->cardNumber = $open->getCardNumber();
        $this->currencyCode = 'GBP'; // Hard coded as we only deal with GBP, sure we won't regret this later...

        $this->totalFunds = new Money(0, 'GBP');
        $this->authorizedFunds = new Money(0, 'GBP');
    }

    /**
     * @param Money $withFunds
     */
    public function topUp(Money $withFunds)
    {
        if (!$this->isOpen()) {
            throw new InvalidEventException("Must open an account before adding funds");
        }

        if ($withFunds->getAmount() <= 0) {
            throw new InvalidEventException("Can only top up with positive amount");
        }

        if ($withFunds->getCurrencyCode() !== $this->currencyCode) {
            throw new InvalidEventException("Can not top up with amount in wrong currency", 0, new CurrencyMismatchException());
        }

        $this->play(new TopUp($this->id, $this->version + 1, $withFunds));
    }

    /**
     * @param TopUp $topUp
     */
    public function onTopUp(TopUp $topUp)
    {
        $this->totalFunds = $this->totalFunds->add($topUp->getTopUpAmount());
    }

    /**
     * @param UuidInterface $merchantId
     * @param UuidInterface $transactionId
     * @param Money $amount
     */
    public function authorize(UuidInterface $merchantId, UuidInterface $transactionId, Money $amount)
    {
        if (!$this->isOpen()) {
            throw new InvalidEventException("Must open an account before authorizing");
        }

        if ($amount->getCurrencyCode() !== $this->currencyCode) {
            throw new InvalidEventException("Can not authorize amount in wrong currency", 0, new CurrencyMismatchException());
        }

        if ($this->getAvailableFunds()->sub($amount)->getAmount() < 0) {
            throw new InvalidEventException("Invalid funds to cover authorization");
        }

        if ($this->authorizedTransactions->containsKey($transactionId->toString())) {
            throw new InvalidEventException("Non unique transaction id");
        }

        $this->play(new Authorize($this->id, $this->version + 1, $merchantId, $transactionId, $amount));
    }

    /**
     * @param Authorize $authorize
     */
    public function onAuthorize(Authorize $authorize)
    {
        $this->authorizedFunds = $this->authorizedFunds->add($authorize->getAmount());

        $authorization = new Authorization(
            $authorize->getId(),
            $authorize->getMerchantId(),
            $authorize->getTransactionId(),
            $authorize->getAmount()
        );

        $this->authorizedTransactions->set($authorize->getTransactionId()->toString(), $authorization);
    }

    /**
     * @param UuidInterface $merchantId
     * @param UuidInterface $transactionId
     * @param Money $amount
     */
    public function reverse(UuidInterface $merchantId, UuidInterface $transactionId, Money $amount)
    {
        if (!$this->isOpen()) {
            throw new InvalidEventException("Must open an account before reversing");
        }

        $authorization = $this->findAuthorization($merchantId, $transactionId);

        if ($amount->getCurrencyCode() !== $this->currencyCode) {
            throw new InvalidEventException("Can not reverse transaction in wrong currency", 0, new CurrencyMismatchException());
        }

        if (!$authorization) {
            throw new InvalidEventException("Could not match merchant and transaction and an existing authorization");
        }

        if ($authorization->reverse($amount)->getAmount()->getAmount() < 0) {
            throw new InvalidEventException("Can not reverse more than the authorized amount");
        }

        $this->play(new Reverse($this->id, $this->version + 1, $merchantId, $transactionId, $amount));
    }

    /**
     * @param Reverse $event
     */
    public function onReverse(Reverse $event)
    {
        $authorization = $this->findAuthorization($event->getMerchantId(), $event->getTransactionId());

        if (!$authorization) {
            throw new \RuntimeException("Received reverse event than cannot be played");
        }

        $this->authorizedFunds = $this->authorizedFunds->sub($event->getAmount());

        $authorization = $authorization->reverse($event->getAmount());
        $this->authorizedTransactions->set($event->getTransactionId()->toString(), $authorization);
    }

    /**
     * @param UuidInterface $merchantId
     * @param UuidInterface $transactionId
     * @param Money $amount
     */
    public function capture(UuidInterface $merchantId, UuidInterface $transactionId, Money $amount)
    {
        if (!$this->isOpen()) {
            throw new InvalidEventException("Cannot capture on a closed account");
        }

        if ($amount->getCurrencyCode() !== $this->currencyCode) {
            throw new InvalidEventException("Cannot capture in this currency", 0, new CurrencyMismatchException());
        }

        if (!$authorization = $this->findAuthorization($merchantId, $transactionId)) {
            throw new InvalidEventException("Cannot capture a transaction that was not authorized");
        }

        if ($authorization->getAmount()->lt($amount)) {
            throw new InvalidEventException("Cannot capture more money than has been authorized");
        }

        $this->play(new CaptureAuthorization($this->id, $this->version + 1, $merchantId, $transactionId, $amount));
    }

    /**
     * @param CaptureAuthorization $capture
     */
    public function onCapture(CaptureAuthorization $capture)
    {
        $authorization = $this->findAuthorization($capture->getMerchantId(), $capture->getTransactionId());

        if (!$authorization) {
            throw new \RuntimeException("Attempting to play an invalid capture event, no matching authorization");
        }

        $this->authorizedTransactions->set($capture->getTransactionId()->toString(), $authorization->capture($capture->getAmount()));

        $this->authorizedFunds = $this->authorizedFunds->sub($capture->getAmount());
        $this->totalFunds = $this->totalFunds->sub($capture->getAmount());

        $captureRecord = $this->findCapture($capture->getMerchantId(), $capture->getTransactionId());

        // Oh god I've done capture.capture(capture capture capture).....
        $this->upsertCapture($capture->getTransactionId(), $captureRecord->capture($capture->getAmount()));
    }

    /**
     * @param UuidInterface $merchantId
     * @param UuidInterface $transactionId
     * @param Money $amount
     */
    public function refund(UuidInterface $merchantId, UuidInterface $transactionId, Money $amount)
    {
        if (!$this->isOpen()) {
            throw new InvalidEventException("Cannot process refund against closed account");
        }

        if ($amount->getCurrencyCode() !== $this->currencyCode) {
            throw new InvalidEventException("Cannot process refund in this currency", 0, new CurrencyMismatchException());
        }

        $capture = $this->findCapture($merchantId, $transactionId);

        if ($capture->refund($amount)->getAmount()->getAmount() < 0) {
            throw new InvalidEventException("Cannot process refund for more than captured amount");
        }

        $this->play(new Refund($this->id, $this->version + 1, $merchantId, $transactionId, $amount));
    }

    /**
     * @param Refund $refund
     */
    public function onRefund(Refund $refund)
    {
        $capture = $this->findCapture($refund->getMerchantId(), $refund->getTransactionId());

        $this->upsertCapture($refund->getTransactionId(), $capture->refund($refund->getAmount()));

        $this->totalFunds = $this->totalFunds->add($refund->getAmount());
    }

    /**
     * Play an event without adding it the events collection on this object,
     * for getting the object into the correct state when rebuilding
     *
     * @param AccountEvent $accountEvent
     */
    public function replay(AccountEvent $accountEvent)
    {
        switch (get_class($accountEvent)) {
            case Open::class:
                /** @var Open $accountEvent */
                $this->onOpen($accountEvent);

                break;

            case TopUp::class:
                /** @var TopUp $accountEvent */
                $this->onTopUp($accountEvent);

                break;

            case Authorize::class:
                /** @var Authorize $accountEvent */
                $this->onAuthorize($accountEvent);

                break;

            case Reverse::class:
                /** @var Reverse $accountEvent */
                $this->onReverse($accountEvent);

                break;

            case CaptureAuthorization::class:
                /** @var CaptureAuthorization $accountEvent */
                $this->onCapture($accountEvent);

                break;

            case Refund::class:
                /** @var Refund $accountEvent */
                $this->onRefund($accountEvent);

                break;

            default:
                throw new UnknownEventException(get_class($accountEvent) . " is not a known event");
        }

        $this->version = $accountEvent->getVersion();
    }

    /**
     * @return Collection|AccountEvent[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    /**
     * @return Collection
     */
    public function popEvents(): Collection
    {
        $events = $this->events;

        $this->events = new ArrayCollection();

        return $events;
    }

    /**
     * @param AccountEvent $accountEvent
     */
    private function play(AccountEvent $accountEvent)
    {
        $this->replay($accountEvent);

        $this->events->add($accountEvent);
    }

    /**
     * @param UuidInterface $merchantId
     * @param UuidInterface $transactionId
     *
     * @return Authorization|null
     */
    private function findAuthorization(UuidInterface $merchantId, UuidInterface $transactionId)
    {
        if (!$this->authorizedTransactions->containsKey($transactionId->toString())) {
            return null;
        }

        /** @var Authorization $authorization */
        $authorization = $this->authorizedTransactions->get($transactionId->toString());

        if (!$authorization->getMerchantId()->equals($merchantId)) {
            return null;
        }

        return $authorization;
    }

    /**
     * @param UuidInterface $merchantId
     * @param UuidInterface $transactionId
     *
     * @return Capture
     */
    private function findCapture(UuidInterface $merchantId, UuidInterface $transactionId): Capture
    {
        if (!$this->capturedTransactions->containsKey($transactionId->toString())) {
            return new Capture($merchantId, $transactionId, new Money(0, $this->currencyCode));
        }

        /** @var Capture $capture */
        $capture = $this->capturedTransactions->get($transactionId->toString());

        if (!$capture->getMerchantId()->equals($merchantId)) {
            throw new \InvalidArgumentException("Incorrect merchant id for transaction during capture lookup");
        }

        return $capture;
    }

    /**
     * @param UuidInterface $transactionId
     * @param Capture $capture
     */
    private function upsertCapture(UuidInterface $transactionId, Capture $capture)
    {
        $this->capturedTransactions->set($transactionId->toString(), $capture);
    }
}
