<?php
declare(strict_types=1);

namespace Curve\Card\Account;

use Curve\Card\Money\Money;
use Ramsey\Uuid\UuidInterface;

class Authorize extends AccountEvent
{
    /**
     * @var UuidInterface
     */
    private $merchantId;

    /**
     * @var UuidInterface
     */
    private $transactionId;

    /**
     * @var Money
     */
    private $amount;

    /**
     * Authorize constructor.
     *
     * @param UuidInterface $accountId
     * @param int $version
     * @param UuidInterface $merchantId
     * @param UuidInterface $transactionId
     * @param Money $amount
     */
    public function __construct(
        UuidInterface $accountId,
        int $version,
        UuidInterface $merchantId,
        UuidInterface $transactionId,
        Money $amount
    ) {
        parent::__construct($accountId, $version);

        $this->merchantId = $merchantId;
        $this->transactionId = $transactionId;
        $this->amount = $amount;
    }

    /**
     * @return UuidInterface
     */
    public function getMerchantId(): UuidInterface
    {
        return $this->merchantId;
    }

    /**
     * @return UuidInterface
     */
    public function getTransactionId(): UuidInterface
    {
        return $this->transactionId;
    }

    /**
     * @return Money
     */
    public function getAmount(): Money
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return sprintf("Authorized a payment of %s to merchant %s", $this->amount, $this->merchantId);
    }
}
