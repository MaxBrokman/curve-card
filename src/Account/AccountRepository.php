<?php
declare(strict_types=1);

namespace Curve\Card\Account;

use Ramsey\Uuid\UuidInterface;

interface AccountRepository
{
    /**
     * @param UuidInterface $id
     *
     * @return Account|null
     */
    public function find(UuidInterface $id);

    /**
     * Get an open account.
     *
     * @param UuidInterface $id
     * @param string $cardNumber used if the account doesn't exist
     *
     * @return Account
     */
    public function findOrCreate(UuidInterface $id, string $cardNumber): Account;

    /**
     * @param Account $account
     *
     * @return void
     */
    public function save(Account $account);
}
