<?php
declare(strict_types=1);

namespace Curve\Card\Account;

use Curve\Card\Money\Money;
use Ramsey\Uuid\UuidInterface;

class TopUp extends AccountEvent
{
    /**
     * @var Money
     */
    private $topUpAmount;

    /**
     * TopUp constructor.
     *
     * @param UuidInterface $accountId
     * @param int $version
     * @param Money $topUpAmount
     */
    public function __construct(UuidInterface $accountId, int $version, Money $topUpAmount)
    {
        parent::__construct($accountId, $version);

        $this->topUpAmount = $topUpAmount;
    }

    /**
     * @return Money
     */
    public function getTopUpAmount(): Money
    {
        return $this->topUpAmount;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return sprintf("Topped up account with %s", $this->topUpAmount);
    }
}
