<?php
declare(strict_types=1);

namespace Curve\Card\Account;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

abstract class AccountEvent
{
    /**
     * @var UuidInterface
     */
    protected $id;

    /**
     * @var UuidInterface
     */
    private $accountId;

    /**
     * @var int
     */
    protected $version;

    /**
     * AccountEvent constructor.
     *
     * @param UuidInterface $accountId
     * @param int $version
     */
    public function __construct(UuidInterface $accountId, int $version)
    {
        $this->id = Uuid::uuid4();
        $this->accountId = $accountId;
        $this->version = $version;
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return UuidInterface
     */
    public function getAccountId(): UuidInterface
    {
        return $this->accountId;
    }

    /**
     * @return int
     */
    public function getVersion(): int
    {
        return $this->version;
    }

    /**
     * For displaying to the user their account history
     *
     * @return string
     */
    abstract public function __toString(): string;
}
