<?php
declare(strict_types=1);

namespace Curve\Card\Account;

use Ramsey\Uuid\UuidInterface;

class Open extends AccountEvent
{
    /**
     * @var string
     */
    private $cardNumber;

    public function __construct(UuidInterface $accountId, string $cardNumber, int $version)
    {
        parent::__construct($accountId, $version);

        $this->cardNumber = $cardNumber;
    }

    /**
     * @return string
     */
    public function getCardNumber(): string
    {
        return $this->cardNumber;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return sprintf("Opened account %s", $this->getAccountId());
    }
}
