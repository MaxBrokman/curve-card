<?php


namespace Curve\Card\Account;

use Ramsey\Uuid\UuidInterface;

interface AccountEventRepository
{
    /**
     * @param UuidInterface $accountId
     *
     * @return AccountEvent[]
     */
    public function getEventsFor(UuidInterface $accountId): array;
}
