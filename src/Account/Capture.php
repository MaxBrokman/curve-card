<?php


namespace Curve\Card\Account;

use Curve\Card\Money\Money;
use Ramsey\Uuid\UuidInterface;

class Capture
{
    /**
     * @var UuidInterface
     */
    private $merchantId;

    /**
     * @var UuidInterface
     */
    private $transactionId;

    /**
     * @var Money
     */
    private $amount;

    /**
     * Capture constructor.
     *
     * @param UuidInterface $merchantId
     * @param UuidInterface $transactionId
     * @param Money $amount
     */
    public function __construct(UuidInterface $merchantId, UuidInterface $transactionId, Money $amount)
    {
        $this->merchantId = $merchantId;
        $this->transactionId = $transactionId;
        $this->amount = $amount;
    }

    /**
     * @return UuidInterface
     */
    public function getMerchantId(): UuidInterface
    {
        return $this->merchantId;
    }

    /**
     * @return UuidInterface
     */
    public function getTransactionId(): UuidInterface
    {
        return $this->transactionId;
    }

    /**
     * @return Money
     */
    public function getAmount(): Money
    {
        return $this->amount;
    }

    /**
     * @param Money $amount
     *
     * @return Capture
     */
    public function capture(Money $amount): Capture
    {
        return new Capture($this->merchantId, $this->transactionId, $this->amount->add($amount));
    }

    /**
     * @param Money $amount
     *
     * @return Capture
     */
    public function refund(Money $amount): Capture
    {
        return new Capture($this->merchantId, $this->transactionId, $this->amount->sub($amount));
    }
}
