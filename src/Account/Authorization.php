<?php
declare(strict_types=1);

namespace Curve\Card\Account;

use Curve\Card\Money\Money;
use Ramsey\Uuid\UuidInterface;

class Authorization
{
    /**
     * @var UuidInterface
     */
    private $eventId;

    /**
     * @var UuidInterface
     */
    private $merchantId;

    /**
     * @var UuidInterface
     */
    private $transactionId;

    /**
     * @var Money
     */
    private $amount;

    /**
     * @param UuidInterface $eventId
     * @param UuidInterface $merchantId
     * @param UuidInterface $transactionId
     * @param Money $amount
     */
    public function __construct(
        UuidInterface $eventId,
        UuidInterface $merchantId,
        UuidInterface $transactionId,
        Money $amount
    ) {
        $this->eventId = $eventId;
        $this->merchantId = $merchantId;
        $this->transactionId = $transactionId;
        $this->amount = $amount;
    }

    /**
     * @return UuidInterface
     */
    public function getEventId(): UuidInterface
    {
        return $this->eventId;
    }

    /**
     * @return UuidInterface
     */
    public function getMerchantId(): UuidInterface
    {
        return $this->merchantId;
    }

    /**
     * @return UuidInterface
     */
    public function getTransactionId(): UuidInterface
    {
        return $this->transactionId;
    }

    /**
     * @return Money
     */
    public function getAmount(): Money
    {
        return $this->amount;
    }

    /**
     * @param Money $amount
     *
     * @return Authorization
     */
    public function reverse(Money $amount): Authorization
    {
        return new Authorization(
            $this->eventId,
            $this->merchantId,
            $this->transactionId,
            $this->amount->sub($amount)
        );
    }

    /**
     * @param Money $amount
     *
     * @return Authorization
     */
    public function capture(Money $amount): Authorization
    {
        return new Authorization(
            $this->eventId,
            $this->merchantId,
            $this->transactionId,
            $this->amount->sub($amount)
        );
    }
}
