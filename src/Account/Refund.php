<?php


namespace Curve\Card\Account;

use Curve\Card\Money\Money;
use Ramsey\Uuid\UuidInterface;

class Refund extends AccountEvent
{
    /**
     * @var UuidInterface
     */
    private $merchantId;

    /**
     * @var UuidInterface
     */
    private $transactionId;

    /**
     * @var Money
     */
    private $amount;

    /**
     * Refund constructor.
     *
     * @param UuidInterface $accountId
     * @param int $version
     * @param UuidInterface $merchantId
     * @param UuidInterface $transactionId
     * @param Money $amount
     */
    public function __construct(
        UuidInterface $accountId,
        int $version,
        UuidInterface $merchantId,
        UuidInterface $transactionId,
        Money $amount
    ) {
        parent::__construct($accountId, $version);

        $this->merchantId = $merchantId;
        $this->transactionId = $transactionId;
        $this->amount = $amount;
    }

    /**
     * @return UuidInterface
     */
    public function getMerchantId(): UuidInterface
    {
        return $this->merchantId;
    }

    /**
     * @return UuidInterface
     */
    public function getTransactionId(): UuidInterface
    {
        return $this->transactionId;
    }

    /**
     * @return Money
     */
    public function getAmount(): Money
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return sprintf("Merchant %s refunded %s from transaction %s", $this->merchantId, $this->amount, $this->transactionId);
    }
}
