<?php

/**
 * @return \Slim\App
 */
function app(): \Slim\App
{
    return \Curve\Card\Http\AppContainer::getApp();
}

/**
 * @return \League\Container\Container
 */
function container(): \League\Container\Container
{
    return app()->getContainer();
}

/**
 * @return \Slim\Views\Twig
 */
function view(): \Slim\Views\Twig
{
    return container()->get('view');
}

/**
 * @param $name
 * @param $params
 *
 * @return string
 */
function route($name, $params): string
{
    return container()->get('router')->pathFor($name, $params);
}
