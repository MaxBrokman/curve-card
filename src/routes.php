<?php
use Curve\Card\Http\AccountController;
use Curve\Card\Http\HomeController;

// Account home page
$app->get('/', HomeController::class . ":splash")->setName('home');
$app->get('/{accountId}', AccountController::class . ":home")->setName('accountHome');
$app->post('/{accountId}/topup', AccountController::class . ":topup")->setName('accountTopup');
$app->post('/{accountId}/authorize', AccountController::class . ":authorize")->setName('accountAuthorize');
$app->post('/{accountId}/capture', AccountController::class . ":capture")->setName('accountCapture');
$app->post('/{accountId}/reverse', AccountController::class . ":reverse")->setName('accountReverse');
$app->post('/{accountId}/refund', AccountController::class . ":refund")->setName('accountRefund');
