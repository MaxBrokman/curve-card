<?php
declare(strict_types=1);

namespace Curve\Card\Persistence;

use Curve\Card\Account\AccountEvent;
use Ramsey\Uuid\UuidInterface;

class PersistableAccountEvent
{
    /**
     * @var UuidInterface
     */
    private $id;

    /**
     * @var UuidInterface
     */
    private $accountId;

    /**
     * @var int
     */
    private $version;

    /**
     * @var string
     */
    private $serializedEvent;

    /**
     * PersistableAccountEvent constructor.
     *
     * @param UuidInterface $id
     * @param UuidInterface $accountId
     * @param int $version
     * @param string $serializedEvent
     */
    public function __construct(UuidInterface $id, UuidInterface $accountId, int $version, string $serializedEvent)
    {
        $this->id = $id;
        $this->accountId = $accountId;
        $this->version = $version;
        $this->serializedEvent = $serializedEvent;
    }

    /**
     * @param AccountEvent $accountEvent
     *
     * @return static
     */
    public static function fromAccountEvent(AccountEvent $accountEvent)
    {
        return new static($accountEvent->getId(), $accountEvent->getAccountId(), $accountEvent->getVersion(),
            serialize($accountEvent));
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return UuidInterface
     */
    public function getAccountId(): UuidInterface
    {
        return $this->accountId;
    }

    /**
     * @return int
     */
    public function getVersion(): int
    {
        return $this->version;
    }

    /**
     * @return string
     */
    public function getSerializedEvent(): string
    {
        return $this->serializedEvent;
    }

    /**
     * @return AccountEvent
     */
    public function toEvent()
    {
        return unserialize($this->serializedEvent);
    }
}
