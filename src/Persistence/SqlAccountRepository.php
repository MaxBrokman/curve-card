<?php
declare(strict_types=1);

namespace Curve\Card\Persistence;

use Aura\Sql\ExtendedPdo;
use Aura\SqlQuery\Common\Insert;
use Aura\SqlQuery\QueryFactory;
use Curve\Card\Account\Account;
use Curve\Card\Account\AccountEvent;
use Curve\Card\Account\AccountRepository;
use Ramsey\Uuid\UuidInterface;

class SqlAccountRepository implements AccountRepository
{

    /**
     * @var ExtendedPdo
     */
    private $pdo;

    /**
     * @var SqlAccountEventRepository
     */
    private $events;

    /**
     * @param ExtendedPdo $pdo
     * @param SqlAccountEventRepository $events
     */
    public function __construct(ExtendedPdo $pdo, SqlAccountEventRepository $events)
    {
        $this->pdo = $pdo;
        $this->events = $events;
    }

    /**
     * @param UuidInterface $id
     *
     * @return Account|null
     */
    public function find(UuidInterface $id)
    {
        $events = $this->events->getEventsFor($id);

        if (!$events) {
            return null;
        }

        $account = new Account();

        foreach ($events as $event) {
            $account->replay($event);
        }

        return $account;
    }

    /**
     * @param UuidInterface $accountId
     * @param string $cardNumber
     *
     * @return Account
     */
    public function findOrCreate(UuidInterface $accountId, string $cardNumber): Account
    {
        $account = $this->find($accountId);

        if ($account) {
            return $account;
        }

        $account = new Account();
        $account->open($accountId, $cardNumber);

        $this->save($account);

        return $account;
    }

    /**
     * @param Account $account
     *
     * @return void
     */
    public function save(Account $account)
    {
        /** @var PersistableAccountEvent[] $persistableEvents */
        $persistableEvents = array_map(function (AccountEvent $event) {
            return PersistableAccountEvent::fromAccountEvent($event);
        }, $account->getEvents()->toArray());

        $queryFactory = new QueryFactory('sqlite', QueryFactory::COMMON);

        /** @var Insert $insert */
        $insert = $queryFactory->newInsert();

        $insert->into("account_events");

        foreach ($persistableEvents as $persistableEvent) {
            $insert->addRow([
                'id' => $persistableEvent->getId()->toString(),
                'account_id' => $persistableEvent->getAccountId()->toString(),
                'version' => $persistableEvent->getVersion(),
                'serialized' => $persistableEvent->getSerializedEvent(),
            ]);
        }

        $statement = $this->pdo->prepare($insert->getStatement());
        $statement->execute($insert->getBindValues());

        $account->popEvents();
    }
}
