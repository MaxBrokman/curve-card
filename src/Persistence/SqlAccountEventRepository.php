<?php
declare(strict_types=1);

namespace Curve\Card\Persistence;

use Aura\Sql\ExtendedPdo;
use Curve\Card\Account\AccountEvent;
use Curve\Card\Account\AccountEventRepository;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class SqlAccountEventRepository implements AccountEventRepository
{

    /**
     * @var ExtendedPdo
     */
    private $pdo;

    /**
     * @param ExtendedPdo $pdo
     */
    public function __construct(ExtendedPdo $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @param UuidInterface $accountId
     *
     * @return AccountEvent[]
     */
    public function getEventsFor(UuidInterface $accountId): array
    {
        $query = "SELECT * FROM account_events WHERE account_id = ? ORDER BY version ASC";
        $events = $this->pdo->fetchAll($query, [$accountId->toString()]);

        return array_map(function (array $event) {
            $persistableEvent = new PersistableAccountEvent(
                Uuid::fromString($event['id']),
                Uuid::fromString($event['account_id']),
                (int)$event['version'],
                $event['serialized']
            );

            return $persistableEvent->toEvent();
        }, $events);
    }
}
