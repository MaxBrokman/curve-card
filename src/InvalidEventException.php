<?php
declare(strict_types=1);

namespace Curve\Card;

class InvalidEventException extends \LogicException
{
}
