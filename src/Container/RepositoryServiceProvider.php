<?php


namespace Curve\Card\Container;

use Curve\Card\Account\AccountEventRepository;
use Curve\Card\Account\AccountRepository;
use Curve\Card\Persistence\SqlAccountEventRepository;
use Curve\Card\Persistence\SqlAccountRepository;
use League\Container\ServiceProvider\AbstractServiceProvider;

class RepositoryServiceProvider extends AbstractServiceProvider
{
    protected $provides = [
        AccountRepository::class,
    ];

    /**
     * Use the register method to register items with the container via the
     * protected $this->container property or the `getContainer` method
     * from the ContainerAwareTrait.
     *
     * @return void
     */
    public function register()
    {
        $this->getContainer()->add(AccountRepository::class, function () {
            return $this->getContainer()->get(SqlAccountRepository::class);
        });

        $this->getContainer()->add(AccountEventRepository::class, function () {
            return $this->getContainer()->get(SqlAccountEventRepository::class);
        });
    }
}
