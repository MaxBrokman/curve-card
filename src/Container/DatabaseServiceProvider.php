<?php


namespace Curve\Card\Container;

use Aura\Sql\ExtendedPdo;
use League\Container\ServiceProvider\AbstractServiceProvider;

class DatabaseServiceProvider extends AbstractServiceProvider
{
    protected $provides = [
        ExtendedPdo::class,
    ];

    /**
     * Use the register method to register items with the container via the
     * protected $this->container property or the `getContainer` method
     * from the ContainerAwareTrait.
     *
     * @return void
     */
    public function register()
    {
        $this->getContainer()->share(ExtendedPdo::class, function () {
            return new ExtendedPdo(
                getenv('DB_DSN'),
                getenv('DB_USER'),
                getenv('DB_PASS'),
                [],
                []
            );
        });
    }
}
