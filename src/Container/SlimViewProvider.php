<?php


namespace Curve\Card\Container;

use League\Container\ServiceProvider\AbstractServiceProvider;
use Slim\Views\Twig;
use Slim\Views\TwigExtension;

class SlimViewProvider extends AbstractServiceProvider
{
    protected $provides = [
        'view'
    ];


    /**
     * Use the register method to register items with the container via the
     * protected $this->container property or the `getContainer` method
     * from the ContainerAwareTrait.
     *
     * @return void
     */
    public function register()
    {
        $this->getContainer()->share('view', function () {
            $container = $this->getContainer();

            $view = new Twig(__DIR__ . "/../../templates", [
                'cache' => false
            ]);

            // Instantiate and add Slim specific extension
            $basePath  = rtrim(str_ireplace('index.php', '', $container->get('request')->getUri()->getBasePath()), '/');
            $view->addExtension(new TwigExtension($container->get('router'), $basePath));

            return $view;
        });
    }
}
