<?php


namespace Curve\Card\Container;

use Jenssegers\Lean\SlimServiceProvider;
use League\Container\Container;
use League\Container\ReflectionContainer;

class ContainerFactory
{
    public static $container;

    /**
     * @return Container
     */
    public function getContainer()
    {
        if (static::$container) {
            return static::$container;
        }

        $container = new Container();

        // Setup an autowiring delegate because I'm lazy
        $container->delegate(
            new ReflectionContainer()
        );

        // Bind each service provider manually to the container, with a small app like this
        // we can get away without a big config file.

        // ExtendedPDO etc
        $container->addServiceProvider(new DatabaseServiceProvider);

        // Repository bindings
        $container->addServiceProvider(new RepositoryServiceProvider);

        // Slim setup
        $container->addServiceProvider(new SlimServiceProvider);
        $container->addServiceProvider(new SlimViewProvider);

        static::$container = $container;

        return $container;
    }

    public function reset()
    {
        static::$container = null;
    }
}
