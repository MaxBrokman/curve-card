<?php
declare(strict_types=1);

namespace Curve\Card\Money;

class Money
{
    /**
     * @var int
     */
    private $amount;

    /**
     * @var string
     */
    private $currencyCode;

    /**
     * @param int $amount measure in smalled currency unit, e.g pence
     * @param string $currencyCode
     */
    public function __construct(int $amount, string $currencyCode)
    {
        $this->amount       = $amount;
        $this->currencyCode = $currencyCode;
    }

    /**
     * Create a money object from the base unit, rather than the smallest unit.
     *
     * Rounds down any fraction smaller than smallest unit
     *
     * @param float $amount
     * @param string $currencyCode
     *
     * @return Money
     */
    public static function fromBase(float $amount, string $currencyCode)
    {
        $integerAmount = intval(floor($amount * 100));
        return new Money($integerAmount, $currencyCode);
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getCurrencyCode(): string
    {
        return $this->currencyCode;
    }

    /**
     * @param Money $money
     *
     * @return Money
     */
    public function add(Money $money): Money
    {
        if ($this->currencyCode !== $money->currencyCode) {
            throw new CurrencyMismatchException($this->currencyCode . " cannot be compared to " . $money->currencyCode);
        }

        return new Money($this->amount + $money->amount, $this->currencyCode);
    }

    /**
     * @param Money $money
     *
     * @return Money
     */
    public function sub(Money $money): Money
    {
        if ($this->currencyCode !== $money->currencyCode) {
            throw new CurrencyMismatchException($this->currencyCode . " cannot be compared to " . $money->currencyCode);
        }

        return new Money($this->amount - $money->amount, $this->currencyCode);
    }

    /**
     * @return Money
     */
    public function negate(): Money
    {
        return new Money(-$this->amount, $this->currencyCode);
    }

    /**
     * @param Money $money
     *
     * @return bool
     */
    public function lt(Money $money): bool
    {
        return $this->amount < $money->amount;
    }

    /**
     * @param Money $money
     *
     * @return bool
     */
    public function equals(Money $money): bool
    {
        return $this->currencyCode === $money->currencyCode && $this->amount === $money->amount;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        // Makes a naughty assumption that all currencies work like decimalised GBP!
        return sprintf("%s %s", number_format($this->amount / 100, 2), $this->currencyCode);
    }
}
