<?php
declare(strict_types=1);

namespace Curve\Card\Money;

class CurrencyMismatchException extends \InvalidArgumentException
{
}
